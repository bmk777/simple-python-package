default:
  image: python:3.9

stages:
  - build
  - test
  - package
  - deploy

variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  PYTHON_PACKAGE_DIR: $CI_PROJECT_DIR/.cache/python-packages
  REQUIREMENTS: requirements.txt
  DEV_REQUIREMENTS: dev.requirements.txt

.pythonpath: &pythonpath
  before_script:
    - export PYTHONPATH="$PYTHON_PACKAGE_DIR"

.cache: &cache
  key: $CI_COMMIT_REF_SLUG
  policy: pull
  paths:
    - $PIP_CACHE_DIR
    - $PYTHON_PACKAGE_DIR

build-dependencies:
  stage: build
  cache:
    <<: *cache
    policy: pull-push
  artifacts:
    expire_in: 1 day
    paths:
      - $PIP_CACHE_DIR
      - $PYTHON_PACKAGE_DIR
  before_script:
    - pip install --upgrade pip
    - rm -rf ${PIP_CACHE_DIR} ${PYTHON_PACKAGE_DIR}
    - export PYTHONPATH="$PYTHON_PACKAGE_DIR"
  script:
    - pip install --progress-bar off --no-cache-dir --target ${PYTHON_PACKAGE_DIR} --requirement ${REQUIREMENTS}
    - pip install --progress-bar off --no-cache-dir --target ${PYTHON_PACKAGE_DIR} --requirement ${DEV_REQUIREMENTS}

test:
  stage: test
  cache:
    <<: *cache
  artifacts:
    when: always
    expire_in: 1 day
    paths:
      - htmlcov
      - .coverage
  coverage: '/Total coverage: \d+.\d+%$/'
  <<: *pythonpath
  script:
    - python -m pytest

build-package:
  stage: package
  cache:
    <<: *cache
  artifacts:
    expire_in: 1 day
    paths:
      - dist
  <<: *pythonpath
  script:
    - python -m build

.release:
  stage: deploy
  when: manual
  cache:
    <<: *cache
  dependencies:
    - build-package
  <<: *pythonpath

release to testpypi:
  extends: .release
  script:
    - python -m twine upload -u ${TEST_PYPI_USERNAME} -p ${TEST_PYPI_PASSWORD} -r testpypi dist/*

release to pypi:
  extends: .release
  script:
    - python -m twine upload -u ${PYPI_USERNAME} -p ${PYPI_PASSWORD} dist/*
